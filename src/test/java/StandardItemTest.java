/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import cz.cvut.eshop.shop.StandardItem;

/**
 *
 * @author just-paja
 */
public class StandardItemTest {    
    @Test
    public void convertsToStringWithPriceAndLoyaltyPoints() {
        StandardItem item = new StandardItem(
                616,
                "Sonic Screwdriver",
                32,
                "Screwdrivers",
                1000
        );
        assertEquals(item.toString(), "Item   ID 616   NAME Sonic Screwdriver   CATEGORY Screwdrivers   PRICE 32.0   LOYALTY POINTS 1000");
    }

    @Test
    public void setsLoyaltyPoints() {
        StandardItem item = new StandardItem(
                616,
                "Sonic Screwdriver",
                32,
                "Screwdrivers",
                1000
        );
        item.setLoyaltyPoints(10);
        assertEquals(item.getLoyaltyPoints(), 10);
    }    
}
